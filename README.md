# Swift コーディングガイド（助川方式）

## 定数

以下、1 internal以上クラス／1 File を原則とする場合。

```swift
import Foundation

// 値渡し型（struct）の場合

// クラス定数はファイルスコープでprivateとして切り出す。
// クラススコープにすると、クラス名.定数名でアクセスする必要があり
// 煩わしいため。
private let TokyoStockExchange	= "TYO"
private let YahooFinanceURL		= "https://download.finance.yahoo.co.jp/common/history/%@.T.csv"
private let ChartCSVHeader		= "日付,始値,高値,安値,終値,出来高,調整後終値"
private let CSVDataCharSet		= CharacterSet(charactersIn: "0123456789-,./")

// Javaで言う所の、Util的な関数はファイルスコープにする。
// 定数と同様の発想。
private func timestamp(date: String, format: String) -> Int? {
	let dateFormatter = DateFormatter()
	dateFormatter.dateFormat = format
	guard let time = dateFormatter.date(from: date)?.timeIntervalSince1970 else { return nil }
	return Int(time)
}

internal final class YahooFinance {
    
    // 参照型（class）は、クラススコープで切ること。

    // 日付フォーマット
    private static let TradeDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()

```